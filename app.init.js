"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppInit = void 0;
const swapi_service_1 = require("./client/rest/swapi/service/swapi.service");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const role_enum_1 = require("./auth/model/enum/role.enum");
const password_encrypter_service_1 = require("./auth/security/util/password.encrypter.service");
const role_entity_1 = require("./user/model/entity/role.entity");
const user_entity_1 = require("./user/model/entity/user.entity");
const role_service_1 = require("./user/service/role.service");
const user_service_1 = require("./user/service/user.service");
const film_service_1 = require("./film/service/film.service");
const film_mapper_1 = require("./film/model/mapper/film.mapper");
let AppInit = exports.AppInit = class AppInit {
    constructor(userService, roleService, configService, crypter, swapiService, filmService, filmMapper) {
        this.userService = userService;
        this.roleService = roleService;
        this.configService = configService;
        this.crypter = crypter;
        this.swapiService = swapiService;
        this.filmService = filmService;
        this.filmMapper = filmMapper;
        this.logger = new common_1.Logger();
    }
    async onModuleInit() {
        await this.init();
    }
    async init() {
        try {
            this.logger.log('Inizializing AppInit.. please wait', 'AppInit');
            let roleAdministrator = new role_entity_1.Role();
            roleAdministrator.name = role_enum_1.RoleEnum.Admin;
            roleAdministrator = await this.roleService.create(roleAdministrator);
            this.logger.log('Inizializing.. Adding admin role', 'AppInit');
            let roleUser = new role_entity_1.Role();
            roleUser.name = role_enum_1.RoleEnum.User;
            roleUser = await this.roleService.create(roleUser);
            this.logger.log('Inizializing.. Adding user role', 'AppInit');
            const admin = new user_entity_1.User();
            admin.username = this.configService.get('ADMIN_USER');
            admin.password = await this.crypter.encrypt(this.configService.get('ADMIN_PASSWORD'));
            admin.roles = [roleAdministrator];
            await this.userService.create(admin);
            this.logger.log('Inizializing.. Adding admin', 'AppInit');
            const promise = this.swapiService.fetchData();
            promise.subscribe(response => {
                var _a, _b;
                console.log(response.data);
                (_b = (_a = response.data) === null || _a === void 0 ? void 0 : _a.results) === null || _b === void 0 ? void 0 : _b.map(dataFilm => {
                    const film = this.filmMapper.toImport(dataFilm);
                    console.log(film);
                    this.filmService.import(film);
                });
            });
            this.logger.log('Inizializing.. Search api data', 'AppInit');
            this.logger.log('Inizializing Complete..', 'AppInit');
        }
        catch (e) {
            this.logger.log(e);
        }
    }
};
exports.AppInit = AppInit = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        role_service_1.RoleService,
        config_1.ConfigService,
        password_encrypter_service_1.PasswordEncrypterService,
        swapi_service_1.SwapiService,
        film_service_1.FilmService,
        film_mapper_1.FilmMapper])
], AppInit);
//# sourceMappingURL=app.init.js.map