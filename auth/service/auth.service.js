"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const user_mapper_1 = require("./../../user/model/mapper/user.mapper");
const jwt_service_1 = require("./jwt.service");
const common_1 = require("@nestjs/common");
const user_service_1 = require("../../user/service/user.service");
const password_encrypter_service_1 = require("../security/util/password.encrypter.service");
let AuthService = exports.AuthService = class AuthService {
    constructor(userService, userMapper, crypter, jwtService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.crypter = crypter;
        this.jwtService = jwtService;
    }
    async signin(signinDTO) {
        const { username, password } = signinDTO;
        const user = await this.userService.findByUsername(username);
        if (user === null || !(await this.crypter.compare(password, user.password))) {
            throw new common_1.UnauthorizedException();
        }
        return this.jwtService.sign(user);
    }
    async signup(signupDTO) {
        const user = await this.userMapper.toCreate(signupDTO);
        return this.userService.create(user).then((user) => {
            return this.userMapper.toDTO(user);
        });
    }
};
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        user_mapper_1.UserMapper,
        password_encrypter_service_1.PasswordEncrypterService,
        jwt_service_1.JwtServicee])
], AuthService);
//# sourceMappingURL=auth.service.js.map