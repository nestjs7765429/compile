import { JwtService } from '@nestjs/jwt';
import { JwtDTO } from '../model/dto/jwt.dto';
import { User } from 'src/user/model/entity/user.entity';
export declare class JwtServicee {
    private jwtService;
    constructor(jwtService: JwtService);
    sign(user: User): JwtDTO;
}
