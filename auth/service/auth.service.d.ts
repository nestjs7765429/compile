import { UserMapper } from './../../user/model/mapper/user.mapper';
import { SignupDTO } from '../model/dto/signup.dto';
import { JwtServicee } from './jwt.service';
import { UserDTO } from 'src/user/model/dto/user.dto';
import { UserService } from 'src/user/service/user.service';
import { JwtDTO } from '../model/dto/jwt.dto';
import { SigninDTO } from '../model/dto/signin.dto';
import { PasswordEncrypterService } from '../security/util/password.encrypter.service';
export declare class AuthService {
    private userService;
    private userMapper;
    private crypter;
    private jwtService;
    constructor(userService: UserService, userMapper: UserMapper, crypter: PasswordEncrypterService, jwtService: JwtServicee);
    signin(signinDTO: SigninDTO): Promise<JwtDTO>;
    signup(signupDTO: SignupDTO): Promise<UserDTO>;
}
