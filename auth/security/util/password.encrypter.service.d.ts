export declare class PasswordEncrypterService {
    encrypt(password: any): Promise<string>;
    compare(password: any, passwordDB: any): Promise<boolean>;
}
