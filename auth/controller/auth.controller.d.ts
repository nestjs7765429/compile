import { AuthService } from '../service/auth.service';
import { SigninDTO } from '../model/dto/signin.dto';
import { SignupDTO } from '../model/dto/signup.dto';
import { JwtDTO } from '../model/dto/jwt.dto';
import { UserDTO } from 'src/user/model/dto/user.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    signin(signinDTO: SigninDTO): Promise<JwtDTO>;
    signup(signupDTO: SignupDTO): Promise<UserDTO>;
}
