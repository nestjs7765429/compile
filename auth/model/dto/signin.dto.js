"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SigninDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class SigninDTO {
}
exports.SigninDTO = SigninDTO;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "The username",
        example: "admin"
    }),
    (0, class_validator_1.IsNotEmpty)({ message: 'The username cant be null' }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], SigninDTO.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: "The password",
        example: "1234"
    }),
    (0, class_validator_1.MinLength)(4),
    (0, class_validator_1.IsNotEmpty)({ message: 'The password cant be null' }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], SigninDTO.prototype, "password", void 0);
//# sourceMappingURL=signin.dto.js.map