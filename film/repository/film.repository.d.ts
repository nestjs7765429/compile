import { Repository } from 'typeorm';
import { Film } from '../model/entity/film.entity';
export declare class FilmRepository {
    private readonly filmRepository;
    constructor(filmRepository: Repository<Film>);
    findAll(): Promise<Film[]>;
    findById(id: number): Promise<Film>;
    create(film: Partial<Film>): Promise<Film>;
    update(film: Partial<Film>): Promise<Film>;
    delete(id: number): Promise<void>;
}
