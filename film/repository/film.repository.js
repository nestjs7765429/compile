"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const film_entity_1 = require("../model/entity/film.entity");
let FilmRepository = exports.FilmRepository = class FilmRepository {
    constructor(filmRepository) {
        this.filmRepository = filmRepository;
    }
    async findAll() {
        return await this.filmRepository.find();
    }
    async findById(id) {
        return await this.filmRepository.findOne({ where: { id } });
    }
    async create(film) {
        const newFilm = this.filmRepository.create({
            ...film,
            id: null
        });
        try {
            return await this.filmRepository.save(newFilm);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'An errors ocurred creating: ' + film.title,
            }, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async update(film) {
        try {
            return await this.filmRepository.save(film);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'An errors ocurred updating: ' + film.title,
            }, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async delete(id) {
        try {
            await this.filmRepository.delete(id);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'An errors ocurred deleting id: ' + id,
            }, common_1.HttpStatus.BAD_REQUEST);
        }
    }
};
exports.FilmRepository = FilmRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(film_entity_1.Film)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], FilmRepository);
//# sourceMappingURL=film.repository.js.map