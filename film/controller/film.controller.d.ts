import { FilmService } from '../service/film.service';
import { FilmDTO } from '../model/dto/film.dto';
import { FilmUpdateDTO } from '../model/dto/film.update.dto';
import { Request } from 'express';
export declare class FilmController {
    private filmService;
    constructor(filmService: FilmService);
    findAll(): Promise<FilmDTO[]>;
    findById(id: number): Promise<FilmDTO>;
    create(filmDTO: FilmDTO, request: Request): Promise<FilmDTO>;
    update(id: number, filmUpdateDTO: FilmUpdateDTO): Promise<FilmDTO>;
    delete(id: number): Promise<void>;
}
