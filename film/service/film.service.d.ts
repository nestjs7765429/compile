import { FilmMapper } from '../model/mapper/film.mapper';
import { FilmDTO } from '../model/dto/film.dto';
import { FilmRepository } from '../repository/film.repository';
import { FilmUpdateDTO } from '../model/dto/film.update.dto';
import { Request } from 'express';
export declare class FilmService {
    private readonly filmRepository;
    private readonly filmMapper;
    constructor(filmRepository: FilmRepository, filmMapper: FilmMapper);
    findAll(): Promise<FilmDTO[]>;
    findById(id: number): Promise<FilmDTO>;
    create(filmDTO: FilmDTO, request: Request): Promise<FilmDTO>;
    update(id: number, filmUpdateDTO: FilmUpdateDTO): Promise<FilmDTO>;
    delete(id: number): Promise<void>;
    import(filmDTO: FilmDTO): Promise<void>;
}
