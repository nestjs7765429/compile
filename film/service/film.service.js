"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmService = void 0;
const film_mapper_1 = require("../model/mapper/film.mapper");
const common_1 = require("@nestjs/common");
const film_repository_1 = require("../repository/film.repository");
let FilmService = exports.FilmService = class FilmService {
    constructor(filmRepository, filmMapper) {
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }
    async findAll() {
        const films = await this.filmRepository.findAll();
        if (!films.length)
            throw new common_1.HttpException('No Content', common_1.HttpStatus.NO_CONTENT);
        return films.map(film => this.filmMapper.toDTO(film));
    }
    async findById(id) {
        const film = await this.filmRepository.findById(id);
        if (!film)
            throw new common_1.NotFoundException();
        return this.filmMapper.toDTO(film);
    }
    async create(filmDTO, request) {
        let film = this.filmMapper.toEntity(filmDTO);
        film = await this.filmRepository.create(film);
        film = this.filmMapper.setUrl(film, request);
        film = await this.filmRepository.update(film);
        return this.filmMapper.toDTO(film);
    }
    async update(id, filmUpdateDTO) {
        let film = await this.filmRepository.findById(id);
        if (!film)
            throw new common_1.NotFoundException();
        film = this.filmMapper.toMerge(film, filmUpdateDTO);
        film = await this.filmRepository.update(film);
        return this.filmMapper.toDTO(film);
    }
    async delete(id) {
        const film = await this.filmRepository.findById(id);
        if (!film)
            throw new common_1.NotFoundException();
        await this.filmRepository.delete(id);
    }
    async import(filmDTO) {
        const film = this.filmMapper.toEntity(filmDTO);
        await this.filmRepository.create(film);
    }
};
exports.FilmService = FilmService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [film_repository_1.FilmRepository,
        film_mapper_1.FilmMapper])
], FilmService);
//# sourceMappingURL=film.service.js.map