"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const film_service_1 = require("./service/film.service");
const film_repository_1 = require("./repository/film.repository");
const film_mapper_1 = require("./model/mapper/film.mapper");
const film_controller_1 = require("./controller/film.controller");
const typeorm_1 = require("@nestjs/typeorm");
const film_entity_1 = require("./model/entity/film.entity");
let FilmModule = exports.FilmModule = class FilmModule {
};
exports.FilmModule = FilmModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([film_entity_1.Film]), config_1.ConfigModule],
        controllers: [film_controller_1.FilmController],
        providers: [film_service_1.FilmService, film_repository_1.FilmRepository, film_mapper_1.FilmMapper],
        exports: [film_service_1.FilmService, film_mapper_1.FilmMapper],
    })
], FilmModule);
//# sourceMappingURL=film.module.js.map