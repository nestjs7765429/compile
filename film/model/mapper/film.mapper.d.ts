import { FilmDTO } from "../dto/film.dto";
import { Film } from "../entity/film.entity";
import { FilmUpdateDTO } from "../dto/film.update.dto";
import { Request } from 'express';
export declare class FilmMapper {
    toEntity(filmDTO: FilmDTO): Film;
    toDTO(film: Film): FilmDTO;
    toMerge(film: Film, filmUpdateDTO: FilmUpdateDTO): Film;
    setUrl(film: Film, request: Request): Film;
    toImport(dataFilm: any): Film;
}
