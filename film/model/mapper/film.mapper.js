"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmMapper = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const film_dto_1 = require("../dto/film.dto");
const film_entity_1 = require("../entity/film.entity");
let FilmMapper = exports.FilmMapper = class FilmMapper {
    toEntity(filmDTO) {
        return (0, class_transformer_1.plainToClass)(film_entity_1.Film, filmDTO);
    }
    ;
    toDTO(film) {
        return (0, class_transformer_1.plainToClass)(film_dto_1.FilmDTO, film);
    }
    ;
    toMerge(film, filmUpdateDTO) {
        const updatedFilm = (0, class_transformer_1.plainToClass)(film_entity_1.Film, filmUpdateDTO);
        return Object.assign(film, updatedFilm);
    }
    ;
    setUrl(film, request) {
        const baseUrl = request.protocol + '://' + request.get('host') + request.originalUrl;
        film.url = `${baseUrl}/${film.id}`;
        return film;
    }
    toImport(dataFilm) {
        const film = dataFilm;
        return film;
    }
    ;
};
exports.FilmMapper = FilmMapper = __decorate([
    (0, common_1.Injectable)()
], FilmMapper);
//# sourceMappingURL=film.mapper.js.map