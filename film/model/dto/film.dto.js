"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class FilmDTO {
}
exports.FilmDTO = FilmDTO;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film id',
        example: 1,
        readOnly: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], FilmDTO.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film title',
        example: 'The Empire Strikes Back'
    }),
    (0, class_validator_1.IsNotEmpty)({ message: 'The title can not be null' }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilmDTO.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film episode number',
        example: 5
    }),
    (0, class_validator_1.IsNotEmpty)({ message: 'The episode can not be null' }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], FilmDTO.prototype, "episode_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film summary',
        example: 'It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed...',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilmDTO.prototype, "opening_crawl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film director',
        example: 'Irvin Kershner',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilmDTO.prototype, "director", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film producer',
        example: 'Gary Kurtz, Rick McCallum',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilmDTO.prototype, "producer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film release date',
        example: '1980-05-17T00:00:00.000Z',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsDateString)(),
    __metadata("design:type", Date)
], FilmDTO.prototype, "release_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/people/1/","https://swapi.dev/api/people/2/"]',
        type: '[String]',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Object)
], FilmDTO.prototype, "characters", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film planets resources as objects',
        example: '["https://swapi.dev/api/planets/1/","https://swapi.dev/api/planets/2/"]',
        type: '[String]',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Object)
], FilmDTO.prototype, "planets", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/starships/1/","https://swapi.dev/api/starships/2/"]',
        type: '[String]',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Object)
], FilmDTO.prototype, "starships", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/vehicles/1/","https://swapi.dev/api/vehicles/2/"]',
        type: '[String]',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Object)
], FilmDTO.prototype, "vehicles", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film characters resources as objects',
        example: '["https://swapi.dev/api/species/1/","https://swapi.dev/api/species/2/"]',
        type: '[String]',
        required: false
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", Object)
], FilmDTO.prototype, "species", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The registry created date',
        example: '2014-12-19T16:52:55.740Z',
        readOnly: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsDateString)(),
    __metadata("design:type", Date)
], FilmDTO.prototype, "created", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The registry updated date',
        example: '2014-12-19T16:52:55.740Z',
        readOnly: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsDateString)(),
    __metadata("design:type", Date)
], FilmDTO.prototype, "edited", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'The film url resource',
        example: 'https://localhost:3000/api/films/4/',
        readOnly: true
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], FilmDTO.prototype, "url", void 0);
//# sourceMappingURL=film.dto.js.map