import { FilmDTO } from './film.dto';
declare const FilmUpdateDTO_base: import("@nestjs/common").Type<Partial<FilmDTO>>;
export declare class FilmUpdateDTO extends FilmUpdateDTO_base {
    id: number;
    title: string;
    episode_id: number;
}
export {};
