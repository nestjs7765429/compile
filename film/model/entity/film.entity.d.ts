export declare class Film {
    id: number;
    title: string;
    episode_id: number;
    opening_crawl: string;
    director: string;
    producer: string;
    release_date: Date;
    characters: object;
    planets: object;
    starships: object;
    vehicles: object;
    species: object;
    created: Date;
    edited: Date;
    url: string;
}
