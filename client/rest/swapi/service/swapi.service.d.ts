import { HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
export declare class SwapiService {
    private readonly configService;
    private readonly httpService;
    constructor(configService: ConfigService, httpService: HttpService);
    fetchData(): import("rxjs").Observable<import("axios").AxiosResponse<any, any>>;
}
