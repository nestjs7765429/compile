"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_init_1 = require("./app.init");
const app_service_1 = require("./app.service");
const auth_module_1 = require("./auth/auth.module");
const client_module_1 = require("./client/client.module");
const film_module_1 = require("./film/film.module");
const film_entity_1 = require("./film/model/entity/film.entity");
const role_entity_1 = require("./user/model/entity/role.entity");
const user_entity_1 = require("./user/model/entity/user.entity");
const user_module_1 = require("./user/user.module");
let AppModule = exports.AppModule = class AppModule {
};
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'sqlite',
                database: ':memory:',
                entities: [user_entity_1.User, role_entity_1.Role, film_entity_1.Film],
                synchronize: true,
            }),
            config_1.ConfigModule.forRoot(),
            auth_module_1.AuthModule,
            film_module_1.FilmModule,
            user_module_1.UsersModule,
            client_module_1.ClientModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, app_init_1.AppInit,],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map