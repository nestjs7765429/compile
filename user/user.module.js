"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersModule = void 0;
const common_1 = require("@nestjs/common");
const role_mapper_1 = require("./model/mapper/role.mapper");
const user_mapper_1 = require("./model/mapper/user.mapper");
const role_repository_1 = require("./repository/role.repository");
const user_repository_1 = require("./repository/user.repository");
const role_service_1 = require("./service/role.service");
const user_service_1 = require("./service/user.service");
const config_1 = require("@nestjs/config");
const password_encrypter_service_1 = require("../auth/security/util/password.encrypter.service");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./model/entity/user.entity");
const role_entity_1 = require("./model/entity/role.entity");
let UsersModule = exports.UsersModule = class UsersModule {
};
exports.UsersModule = UsersModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([user_entity_1.User, role_entity_1.Role])],
        providers: [
            user_service_1.UserService,
            user_repository_1.UserRepository,
            role_service_1.RoleService,
            role_repository_1.RoleRepository,
            user_mapper_1.UserMapper,
            role_mapper_1.RoleMapper,
            password_encrypter_service_1.PasswordEncrypterService,
            config_1.ConfigService,
        ],
        exports: [
            user_service_1.UserService,
            role_service_1.RoleService,
            user_mapper_1.UserMapper,
            role_mapper_1.RoleMapper,
            user_repository_1.UserRepository,
            role_repository_1.RoleRepository,
        ],
    })
], UsersModule);
//# sourceMappingURL=user.module.js.map