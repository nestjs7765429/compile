import { Repository } from 'typeorm';
import { User } from '../model/entity/user.entity';
export declare class UserRepository {
    private readonly userRepository;
    constructor(userRepository: Repository<User>);
    findAll(): Promise<User[]>;
    findByUsername(username: string): Promise<User | null>;
    create(user: Partial<User>): Promise<User>;
}
