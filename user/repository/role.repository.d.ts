import { Repository } from 'typeorm';
import { Role } from '../model/entity/role.entity';
export declare class RoleRepository {
    private readonly roleRepository;
    constructor(roleRepository: Repository<Role>);
    findByName(name: string): Promise<Role | null>;
    create(role: Partial<Role>): Promise<Role>;
}
