"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const role_entity_1 = require("../model/entity/role.entity");
let RoleRepository = exports.RoleRepository = class RoleRepository {
    constructor(roleRepository) {
        this.roleRepository = roleRepository;
    }
    findByName(name) {
        return this.roleRepository.findOneBy({ name: name });
    }
    async create(role) {
        const newRole = this.roleRepository.create({
            ...role,
            id: null,
        });
        try {
            return await this.roleRepository.save(newRole);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.CONFLICT,
                error: error === null || error === void 0 ? void 0 : error.driverError
            }, common_1.HttpStatus.CONFLICT);
        }
    }
};
exports.RoleRepository = RoleRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(role_entity_1.Role)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], RoleRepository);
//# sourceMappingURL=role.repository.js.map