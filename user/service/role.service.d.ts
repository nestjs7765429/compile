import { Role } from '../model/entity/role.entity';
import { RoleRepository } from '../repository/role.repository';
export declare class RoleService {
    private readonly roleRepository;
    constructor(roleRepository: RoleRepository);
    findByName(name: string): Promise<Role | undefined>;
    create(role: Role): Promise<Role | undefined>;
}
