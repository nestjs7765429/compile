import { User } from '../model/entity/user.entity';
import { UserRepository } from '../repository/user.repository';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: UserRepository);
    findByUsername(username: string): Promise<User | undefined>;
    create(user: User): Promise<User | undefined>;
}
