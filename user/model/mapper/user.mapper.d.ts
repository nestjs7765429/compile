import { SignupDTO } from 'src/auth/model/dto/signup.dto';
import { RoleService } from '../../service/role.service';
import { UserDTO } from '../dto/user.dto';
import { User } from '../entity/user.entity';
import { RoleMapper } from './role.mapper';
import { PasswordEncrypterService } from 'src/auth/security/util/password.encrypter.service';
export declare class UserMapper {
    private roleMapper;
    private roleService;
    private crypter;
    constructor(roleMapper: RoleMapper, roleService: RoleService, crypter: PasswordEncrypterService);
    toDTO(user: User): UserDTO;
    toEntity(userDTO: UserDTO): User;
    toCreate(signupDTO: SignupDTO): Promise<User>;
}
