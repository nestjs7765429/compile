"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserMapper = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const role_service_1 = require("../../service/role.service");
const user_dto_1 = require("../dto/user.dto");
const user_entity_1 = require("../entity/user.entity");
const role_mapper_1 = require("./role.mapper");
const password_encrypter_service_1 = require("../../../auth/security/util/password.encrypter.service");
const role_enum_1 = require("../../../auth/model/enum/role.enum");
let UserMapper = exports.UserMapper = class UserMapper {
    constructor(roleMapper, roleService, crypter) {
        this.roleMapper = roleMapper;
        this.roleService = roleService;
        this.crypter = crypter;
    }
    toDTO(user) {
        const userDTO = (0, class_transformer_1.plainToClass)(user_dto_1.UserDTO, user, { excludeExtraneousValues: true });
        userDTO.roles = user.roles.map(role => {
            return this.roleMapper.toDTO(role);
        });
        return userDTO;
    }
    toEntity(userDTO) {
        const user = (0, class_transformer_1.plainToClass)(user_entity_1.User, userDTO, { excludeExtraneousValues: true });
        user.roles = userDTO.roles.map(roleDTO => {
            return this.roleMapper.toEntity(roleDTO);
        });
        return user;
    }
    async toCreate(signupDTO) {
        const user = new user_entity_1.User()
            .setUsername(signupDTO.username)
            .setPassword(await this.crypter.encrypt(signupDTO.password));
        user.roles = [await this.roleService.findByName(role_enum_1.RoleEnum.User)];
        return user;
    }
    ;
};
exports.UserMapper = UserMapper = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [role_mapper_1.RoleMapper,
        role_service_1.RoleService,
        password_encrypter_service_1.PasswordEncrypterService])
], UserMapper);
//# sourceMappingURL=user.mapper.js.map