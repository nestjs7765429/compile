import { RoleDTO } from "../dto/role.dto";
import { Role } from "../entity/role.entity";
export declare class RoleMapper {
    toDTO(roleDTO: Role): RoleDTO;
    toEntity(roleDTO: RoleDTO): Role;
}
