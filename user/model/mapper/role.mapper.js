"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleMapper = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const role_dto_1 = require("../dto/role.dto");
const role_entity_1 = require("../entity/role.entity");
let RoleMapper = exports.RoleMapper = class RoleMapper {
    toDTO(roleDTO) {
        return (0, class_transformer_1.plainToClass)(role_dto_1.RoleDTO, roleDTO, { excludeExtraneousValues: true });
    }
    toEntity(roleDTO) {
        return (0, class_transformer_1.plainToClass)(role_entity_1.Role, roleDTO, { excludeExtraneousValues: true });
    }
};
exports.RoleMapper = RoleMapper = __decorate([
    (0, common_1.Injectable)()
], RoleMapper);
//# sourceMappingURL=role.mapper.js.map