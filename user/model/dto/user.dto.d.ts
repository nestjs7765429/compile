import { RoleDTO } from './role.dto';
export declare class UserDTO {
    id: number;
    username: string;
    roles: RoleDTO[];
}
