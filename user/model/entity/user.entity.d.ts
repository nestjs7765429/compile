import { Role } from './role.entity';
export declare class User {
    id: number;
    username: string;
    password: string;
    roles: Role[];
    setUsername(username: string): this;
    setPassword(password: string): this;
    setRoles(roles?: Role[]): this;
}
