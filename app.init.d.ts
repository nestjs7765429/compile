import { SwapiService } from './client/rest/swapi/service/swapi.service';
import { OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PasswordEncrypterService } from './auth/security/util/password.encrypter.service';
import { RoleService } from './user/service/role.service';
import { UserService } from './user/service/user.service';
import { FilmService } from './film/service/film.service';
import { FilmMapper } from './film/model/mapper/film.mapper';
export declare class AppInit implements OnModuleInit {
    private readonly userService;
    private readonly roleService;
    private readonly configService;
    private readonly crypter;
    private readonly swapiService;
    private readonly filmService;
    private readonly filmMapper;
    constructor(userService: UserService, roleService: RoleService, configService: ConfigService, crypter: PasswordEncrypterService, swapiService: SwapiService, filmService: FilmService, filmMapper: FilmMapper);
    onModuleInit(): Promise<void>;
    init(): Promise<void>;
}
